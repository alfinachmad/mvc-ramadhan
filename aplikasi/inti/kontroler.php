<?php

    /*
    |--------------------------------------------------------------------------
    | MVC Ramadhan - kontroler.php
    | @author   : A.B.Ilham. A, Teddy Syaifuddin, Maja Nurachman
    | @since    : June 2016
    | @codename : ramadhan
    | @todo     : Tugas Pak Muchayan
    |--------------------------------------------------------------------------
    */

    class Kontroler {

        public $db      = null;
        public $data    = null;

        function __construct(){
            $this->openDBConnection();
            $this->loadData();
        }

        private function openDBConnection(){
            $options    = [
                PDO::ATTR_DEFAULT_FETCH_MODE    => PDO::FETCH_OBJ,
                PDO::ATTR_ERRMODE               => PDO::ERRMODE_WARNING
            ];
            if(DB_PASS == null OR DB_USER == null):
                $this->db   = false;
            else:
                $connection = new PDO(DB_TYPE . ':host=' . DB_HOST . ';dbname=' . DB_NAME . ';charset=' . DB_CHARSET, DB_USER, DB_PASS, $options);
                $this->db   = $connection;
            endif;
        }

        public function loadData(){
            require APP . 'data/data.php';
            $this->data = new Data($this->db);
        }

    }