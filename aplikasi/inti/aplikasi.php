<?php

    /*
    |--------------------------------------------------------------------------
    | MVC Ramadhan - aplikasi.php
    | @author   : A.B.Ilham. A, Teddy Syaifuddin, Maja Nurachman
    | @since    : June 2016
    | @codename : ramadhan
    | @todo     : Tugas Pak Muchayan
    |--------------------------------------------------------------------------
    */

    class Aplikasi {

        private $url_controller = null;
        private $url_action     = null;
        private $url_params     = [];

        public function __construct($defaultController=''){
            $this->splitURL();

            if(!$this->url_controller):
                require APP . 'kontroler/'.strtolower($defaultController).'.php';
                $page   = new $defaultController();
                echo $page->index();
            elseif(file_exists(APP . 'kontroler/' . $this->url_controller . '.php')):
                require APP . 'kontroler/' . $this->url_controller . '.php';
                $this->url_controller   = new $this->url_controller();
                if(method_exists($this->url_controller, $this->url_action)):
                    if(!empty($this->url_params)):
                        call_user_func_array([$this->url_controller,$this->url_action], $this->url_params);
                    else:
                        $this->url_controller->{$this->url_action}();
                    endif;
                else:
                    if(strlen($this->url_action) == 0):
                        $this->url_controller->index();
                    else:
                        header('location: ' . URL . 'error');
                    endif;
                endif;
            else:
                header('location: ' . URL . 'error');
            endif;
        }

        private function splitURL(){
            if(isset($_GET['url'])):
                $url = trim($_GET['url'], '/');
                $url = filter_var($url, FILTER_SANITIZE_URL);
                $url = explode('/', $url);

                $this->url_controller   = isset($url[0]) ? $url[0] : null;
                $this->url_action       = isset($url[1]) ? $url[1] : null;

                unset($url[0], $url[1]);

                $this->url_params = array_values($url);
            endif;
        }

    }