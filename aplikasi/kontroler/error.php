<?php

    /*
    |--------------------------------------------------------------------------
    | MVC Ramadhan - error.php
    | @author   : A.B.Ilham. A, Teddy Syaifuddin, Maja Nurachman
    | @since    : June 2016
    | @codename : ramadhan
    | @todo     : Tugas Pak Muchayan
    |--------------------------------------------------------------------------
    */

    class Error extends Kontroler {

        public function index(){
            require APP . 'tampilan/error/index.php';
        }

    }