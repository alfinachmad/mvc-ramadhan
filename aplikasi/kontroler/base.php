<?php

    /*
    |--------------------------------------------------------------------------
    | MVC Ramadhan - base.php
    | @author   : A.B.Ilham. A, Teddy Syaifuddin, Maja Nurachman
    | @since    : June 2016
    | @codename : ramadhan
    | @todo     : Tugas Pak Muchayan
    |--------------------------------------------------------------------------
    */

    class Base extends Kontroler {

        public function index(){
            require APP . 'tampilan/base/index.php';
        }

    }