<?php

    /*
    |--------------------------------------------------------------------------
    | MVC Ramadhan - data.php
    | @author   : A.B.Ilham. A, Teddy Syaifuddin, Maja Nurachman
    | @since    : June 2016
    | @codename : ramadhan
    | @todo     : Tugas Pak Muchayan
    |--------------------------------------------------------------------------
    */

    class Data {

        function __construct($db){
            try {
                $this->db   = $db;
            } catch (PDOException $e){
                exit('Koneksi database tidak terhubung !');
            }
        }

        public function render($class){
            $filename   = APP . 'data/data_' . $class . '.php';
            if(file_exists($filename)):
                require 'data_' . $class . '.php';
                $name   = 'data_' . $class;
                return new $name($this->db);
            else:
                return 'tidak ada __CLASSES__ <strong>data_' . $class;
            endif;
        }

    }