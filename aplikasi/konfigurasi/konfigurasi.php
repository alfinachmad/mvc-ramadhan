<?php

    /*
    |--------------------------------------------------------------------------
    | MVC Ramadhan - konfigurasi.php
    | @author   : A.B.Ilham. A, Teddy Syaifuddin, Maja Nurachman
    | @since    : June 2016
    | @codename : ramadhan
    | @todo     : Tugas Pak Muchayan
    |--------------------------------------------------------------------------
    */

    /*
     * Untuk konfigurasi mode error
     */
    define('ENVIRONMENT', 'development');
    if(ENVIRONMENT == 'development' || ENVIRONMENT == 'dev'):
        error_reporting(E_ALL);
        ini_set('display_errors', 1);
    endif;

    /*
     * Untuk kebutuhan konfigurasi URL
     */
    define('URL_WWW_FOLDER', 'www');
    define('URL_PROTOCOL', 'http://');
    define('URL_DOMAIN', $_SERVER['HTTP_HOST']);
    define('URL_SUB_FOLDER', str_replace(URL_WWW_FOLDER, '', dirname($_SERVER['SCRIPT_NAME'])));
    define('URL', URL_PROTOCOL . URL_DOMAIN . URL_SUB_FOLDER);


    /*
     * Untuk konfigurasi Database
     */
    define('DB_TYPE', 'mysql');
    define('DB_HOST', '127.0.0.1');
    define('DB_NAME', null);
    define('DB_USER', 'root');
    define('DB_PASS', null);
    define('DB_CHARSET', 'utf8');