<?php

    /*
    |--------------------------------------------------------------------------
    | MVC Ramadhan - pustaka.php
    | @author   : A.B.Ilham. A, Teddy Syaifuddin, Maja Nurachman
    | @since    : June 2016
    | @codename : ramadhan
    | @todo     : Tugas Pak Muchayan
    |--------------------------------------------------------------------------
    */

    class Bantuan {

        static public function debugPDO($raw_sql, $param){
            $keys   = [];
            $values = $param;

            foreach ($param as $key => $value):
                if(is_string($key)):
                    $keys[] = '/' . $key . '/';
                else:
                    $keys[] = '/[?]/';
                endif;

                // bring parameter into human-readable format
                if(is_string($value)):
                    $values[$key] = "'" . $value . "'";
                elseif(is_array($value)):
                    $values[$key] = implode(',', $value);
                elseif (is_null($value)):
                    $values[$key] = 'NULL';
                endif;
            endforeach;

            $raw_sql    = preg_replace($keys, $values, $raw_sql, 1, $count);
            return $raw_sql;
        }

    }

