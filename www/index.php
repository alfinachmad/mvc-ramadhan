<?php

    /*
    |--------------------------------------------------------------------------
    | MVC Ramadhan - index.php
    | @author   : A.B.Ilham. A, Teddy Syaifuddin, Maja Nurachman
    | @since    : June 2016
    | @codename : ramadhan
    | @todo     : Tugas Pak Muchayan
    |--------------------------------------------------------------------------
    */

    define('ROOT', dirname(__DIR__) . DIRECTORY_SEPARATOR);
    define('APP', ROOT . 'aplikasi' . DIRECTORY_SEPARATOR);

    require APP . 'konfigurasi/konfigurasi.php';
    require APP . 'pustaka/bantuan.php';

    require APP . 'inti/aplikasi.php';
    require APP . 'inti/kontroler.php';

    // Menentukan default kontroller
    $default    = 'Base';

    // Render halaman
    $app        = new Aplikasi($default);